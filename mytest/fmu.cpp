//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
	laiz->Visible = false;
	FCountWrong = 0;
	FCountCorrect = 0;
	Im1->Visible = false;
    Im2->Visible = false;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
	tc->TabPosition = TTabPosition::None;
    pr->Max = tc->TabCount-1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
		tc->Next();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::AnswerClick(TObject *Sender)
{
	tc->Next();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::AllClick(TObject *Sender)
{
	UnicodeString x;
	x = ((TControl *)Sender)->Tag == 1 ? L"�����" : L"�������";
	if (x == L"�����") {

		((TButton *)Sender)->TextSettings->FontColor =  TAlphaColorRec::Darkgreen;
		FCountCorrect ++;
	}
	if (x == L"�������") {
		((TButton *)Sender)->TextSettings->FontColor = TAlphaColorRec::Brown;
		FCountWrong ++;
	}

	me->Lines->Add(L"������" + tc->ActiveTab->Text + " - " + x);
	laCorrect->Text = Format(L"����� = %d", ARRAYOFCONST((FCountCorrect)));
	laWrong->Text = Format(L"������� = %d", ARRAYOFCONST((FCountWrong)));
	if (tc->ActiveTab->Index == 3) {
		if (FCountCorrect > FCountWrong) {
			me->Lines->Add(L"���� �������");
			Im2->Visible = true;
		}
		else {
			me->Lines->Add(L"���� �� �������");
            Im1->Visible = true;
        }

	}
	tc->Next();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	me->Lines->Clear();
	FCountCorrect = 0;
	FCountWrong = 0;
	Im1->Visible = false;
	Im2->Visible = false;
	tc->First();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	if ((tc->ActiveTab->Index == 0) || (tc->ActiveTab->Index == 4) ) {
		laiz->Visible = false;
	}
	if ((tc->ActiveTab->Index == 1) || (tc->ActiveTab->Index == 2) ||
	(tc->ActiveTab->Index == 3)) {
		laiz->Visible = true;
		laiz->Text = Format(L"(0 �� %d)",
		 ARRAYOFCONST(((tc->ActiveTab->Index))));
	}
	pr->Value = tc->ActiveTab->Index;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage (L"Inc. Dmitry");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button13Click(TObject *Sender){

	if (fm->StyleBook->Tag == 0) {
		fm->StyleBook = StyleBook2;
		fm->StyleBook->Tag = 1;
	}
	else if (fm->StyleBook->Tag == 1) {
		fm->StyleBook = StyleBook1;
        fm->StyleBook->Tag = 0;
	}
}
//---------------------------------------------------------------------------




//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TButton *buAbout;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TButton *buStart;
	TImage *Image1;
	TScrollBox *ScrollBox1;
	TLabel *Label2;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TLabel *Label3;
	TButton *Button1;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TScrollBox *ScrollBox2;
	TLabel *Label4;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TScrollBox *ScrollBox3;
	TButton *buRestart;
	TMemo *me;
	TLayout *Layout2;
	TLabel *laCorrect;
	TLabel *laWrong;
	TProgressBar *pr;
	TLabel *laiz;
	TStyleBook *StyleBook1;
	TImage *Im1;
	TImage *Im2;
	TButton *Button13;
	TStyleBook *StyleBook2;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall AnswerClick(TObject *Sender);
	void __fastcall AllClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
